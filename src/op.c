#include "arch.h"

#define def_op(name) static void op_##name(struct zq32* mach, struct zq32_instruction* in)

def_op(add) {
  uint64_t tmp;

  mach->rf &= ~FLAG_OVRFLW;
  mach->rf &= ~FLAG_NEG;
  mach->rf &= ~FLAG_ZERO;
  mach->rf &= ~FLAG_CARRY;

  mach->rf |= FLAG_OVRFLW * __builtin_add_overflow(mach->r[in->src] , mach->op2, &tmp);
  mach->rf |= FLAG_NEG * ((tmp >> 31) & 1);

  tmp = mach->r[in->src] + mach->op2;

  mach->rf |= FLAG_CARRY * ((tmp >> 32) & 1);

  mach->rf |= FLAG_ZERO * !tmp;

  mach->r[in->dst] = tmp & 0xFFFFFFFF;
}

def_op(sub) {
  uint64_t tmp;

  mach->rf &= ~FLAG_OVRFLW;
  mach->rf &= ~FLAG_NEG;
  mach->rf &= ~FLAG_ZERO;
  mach->rf &= ~FLAG_CARRY;

  mach->rf |= FLAG_OVRFLW * !!__builtin_sub_overflow(mach->r[in->src] , mach->op2, &tmp);
  mach->rf |= FLAG_NEG * ((tmp >> 31) & 1);
  mach->rf |= FLAG_ZERO * !tmp;

  tmp = mach->r[in->src] - mach->op2;

  mach->rf |= FLAG_CARRY * (tmp >> 32);

  mach->r[in->dst] = tmp;
}

def_op(and) {
  mach->rf &= ~FLAG_ZERO;
  mach->rf &= ~FLAG_NEG;

  uint32_t tmp = mach->r[in->src] & mach->op2;

  mach->rf |= FLAG_ZERO * !tmp;
  mach->rf |= FLAG_NEG * (tmp >> 31);

  mach->r[in->dst] = tmp;
}

def_op(or) {
  mach->rf &= ~FLAG_ZERO;
  mach->rf &= ~FLAG_NEG;

  uint32_t tmp = (unsigned)(mach->r[in->src]) | ((unsigned)(mach->op2) & 0xFFFF);

  mach->rf |= FLAG_ZERO * !tmp;
  mach->rf |= FLAG_NEG * (tmp >> 31);

  mach->r[in->dst] = tmp;
}

def_op(xor) {
  mach->rf &= ~FLAG_ZERO;
  mach->rf &= ~FLAG_NEG;

  uint32_t tmp = mach->r[in->src] ^ mach->op2;

  mach->rf |= FLAG_ZERO * !tmp;
  mach->rf |= FLAG_NEG * (tmp >> 31);

  mach->r[in->dst] = tmp;
}

def_op(lod) {
  mach->rf &= ~FLAG_ZERO;
  mach->r[in->dst] = mmio_read(mach, mach->r[in->src] + mach->op2);
  mach->rf |= FLAG_ZERO * !mach->r[in->dst];
}

def_op(str) {
  mmio_write32(mach, mach->r[in->src] + mach->op2, mach->r[in->dst]);
}

def_op(shl) {
  mach->rf &= ~FLAG_CARRY;

  if(!mach->op2)
    return;

  if(mach->op2 > 32) {
    mach->r[in->dst] = 0x00;
  }

  uint64_t tmp = mach->r[in->src];

  tmp <<= mach->op2;

  mach->rf |= FLAG_CARRY * ((tmp >> 31) & 1);

  mach->r[in->dst] = tmp;  
}

def_op(shr) {
  mach->rf &= ~FLAG_CARRY;

  if(!mach->op2)
    return;

  if(mach->op2 > 32) {
    mach->r[in->dst] = 0x00;
  }

  uint32_t tmp = mach->r[in->src];

  tmp >>= mach->op2 - 1;

  mach->rf |= FLAG_CARRY * (tmp & 1);

  mach->r[in->dst] = tmp >> 1;
}

def_op(rol) {
  unsigned int c = mach->op2 & 0b11111;
  mach->r[in->dst] = (mach->r[in->src] << c) | (mach->r[in->src] >> ((-c) & 0b11111));
}

def_op(ror) {
  unsigned int c = mach->op2 & 0b11111;
  mach->r[in->dst] = (mach->r[in->src] >> c) | (mach->r[in->src] << ((-c) & 0b11111));
}

def_op(ldd) {
  mach->rf &= ~FLAG_ZERO;
  uint32_t addr = mach->r[in->src] + mach->op2;
  mach->r[in->dst] = mmio_read(mach, addr) & 0xFFFF;
  mach->rf |= FLAG_ZERO * !mach->r[in->dst];
}

def_op(ldb) {
  mach->rf &= ~FLAG_ZERO;
  uint32_t addr = mach->r[in->src] + mach->op2;
  mach->r[in->dst] = mmio_read(mach, addr) & 0xFF;
  mach->rf |= FLAG_ZERO * !mach->r[in->dst];
}

def_op(std) {
  uint32_t addr = mach->r[in->src] + mach->op2;
  mmio_write16(mach, addr, mach->r[in->dst]);
}

def_op(stb) {
  uint32_t addr = mach->r[in->src] + mach->op2;
  mmio_write8(mach, addr, mach->r[in->dst]);
}

def_op(six) {
  const uint32_t addr = mach->r[in->src]; 
  /* a = [b six c] */
  switch(mach->op2 & 0b11) {
    case 0:
      /* if c = 0, sign extend from bit 7 to bit 15 */
      mach->r[in->dst] = mmio_read(mach, addr) & 0xFF;
      if(mach->r[in->dst] >> 7) {
        mach->r[in->dst] |= 0xFFFFFF00;
      }
    break;
    case 1:
      /* if c = 1, sign extend from bit 15 to bit 31 */
      mach->r[in->dst] = mmio_read(mach, addr) & 0xFFFF;
      if(mach->r[in->dst] >> 15) {
        mach->r[in->dst] |= 0xFFFF0000;
      }
    break;
    case 2: {
      /* if c = 2, sign preserve from bit 31 to 15 */
      uint32_t tmp = mmio_read(mach, addr) & 0xFFFF;
      if(mach->r[in->dst] >> 31) {
        mach->r[in->dst] = tmp |  0xFFFF0000;
      } else {
        mach->r[in->dst] = tmp;
      }
    }
    break;
    case 3: {
      /* if c = 3, sign preserve from bit 31 to 7 */
      uint32_t tmp = mmio_read(mach, addr) & 0xFF;
      if(mach->r[in->dst] >> 31) {
        mach->r[in->dst] = tmp |  0xFFFFFF00;
      } else {
        mach->r[in->dst] = tmp;
      }
    }
    break;
  }
}

zq32_instruction_handler zq32_handler[] = {
  [OP_ADD]  = &op_add,
  [OP_SUB]  = &op_sub,
  [OP_AND]  = &op_and,
  [OP_OR]   = &op_or,
  [OP_XOR]  = &op_xor,
  [OP_LOD]  = &op_lod,
  [OP_STR]  = &op_str,
  [OP_SHL]  = &op_shl,
  [OP_SHR]  = &op_shr,
  [OP_ROL]  = &op_rol,
  [OP_ROR]  = &op_ror,
  [OP_LDD]  = &op_ldd,
  [OP_LDB]  = &op_ldb,
  [OP_STD]  = &op_std,
  [OP_STB]  = &op_stb,
  [OP_SIX]  = &op_six,
};
