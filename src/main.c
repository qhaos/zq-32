#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <SDL2/SDL.h>

#include "zq32.h"

int main(int argc, char* argv[]) {
  if(argc < 2) {
    fprintf(stderr, "Usage: %s bootfile\n", argv[0]);
    exit(EXIT_FAILURE);
  }


  FILE* in = fopen(argv[1], "rb");

  if(!in) {
    perror("fopen");
    exit(EXIT_FAILURE);
  }

  if(fseek(in, 0, SEEK_END) < 0) {
    perror("fseek");
    exit(EXIT_FAILURE);
  }

  long fsize = ftell(in);

  if(fsize < 0) {
    perror("ftell");
    exit(EXIT_FAILURE);
  }

  rewind(in);


  if(SDL_Init(SDL_INIT_VIDEO)) {
    fprintf(stderr, "%s\n", SDL_GetError());
    exit(EXIT_FAILURE);
  }

  struct zq32* mach = zq32_new();

  if(fsize > (signed long)sizeof(mach->ram)) {
    fputs("Warning: file too large to fit in ram, truncated", stderr);
    fsize = sizeof(mach->ram);
  }

  if((signed long)fread(mach->ram, 1, fsize, in) != fsize) {
    perror("fread");
    exit(EXIT_FAILURE);
  }

  while(mach->r[13] != 0xDEADBEEF && zq32_loop(mach))
    ;

  free(mach);
}

