#include <stdbool.h>
#include <stdint.h>
#include <string.h>
#include <stdio.h>
#include "arch.h"
#include "zq32.h"

#ifdef DEBUG
#define dbg_printf(...) printf(stderr, __VA_ARGS__)
#else
#define dbg_printf(...)
#endif
extern zq32_instruction_handler zq32_handler[16];

uint32_t mmio_read(struct zq32* mach, uint32_t addr) {
  /* if the top bit is flipped, it's either vram or kbd buffer */
  if(addr & 0xF0000000) {
    if(addr == (uint32_t) -1) {
      /* kbd read */
      if(mach->kbd_bottom == mach->kbd_top) {
        mach->kbd_bottom = 0;
        mach->kbd_top = 0;
        return 0x00000000;
      }
      return mach->kbd_buff[mach->kbd_bottom++];
    } else {
      addr &= 0xFFFF;
      if(addr >= 320 * 200) {
        return 0x00000000;
      }

      return *(uint32_t*)(&mach->vram[addr]);
    }
  }

  if(addr > (1 << 20) - 1)
    return 0x00000000;

  return *(uint32_t*)(&mach->ram[addr]);
}

#define DEF_MMIO_WRITE(sz)                                                    \
void mmio_write##sz (struct zq32* mach, uint32_t addr, uint##sz##_t  val) {   \
  dbg_printf("%u bytes (%x) written at %.8x\n", sz/8, val, addr);             \
  /* if the top bit is flipped, it's either vram or kbd buffer */             \
  if(addr & 0xF0000000) {                                                     \
    addr &= 0x0000FFFF;                                                       \
    if(addr >= 320 * 200) {                                                   \
      return;                                                                 \
    }                                                                         \
                                                                              \
    memcpy(&mach->vram[addr], &val, sz/8);                                    \
    return;                                                                   \
  }                                                                           \
                                                                              \
  if(addr > (1 << 16) - 1)                                                    \
    return;                                                                   \
                                                                              \
  memcpy(&mach->ram[addr], &val, sz/8);                                       \
}

DEF_MMIO_WRITE(8)
DEF_MMIO_WRITE(16)
DEF_MMIO_WRITE(32)

struct zq32* zq32_new() {
  struct zq32* mach = (struct zq32*) calloc(sizeof(struct zq32), 1);

  if(!mach)
    return mach;

  if(SDL_CreateWindowAndRenderer(320*4, 200*4, 0, &mach->win, &mach->ren)) {
    fprintf(stderr, "%s\n", SDL_GetError());
    exit(EXIT_FAILURE);
  }

  SDL_SetWindowTitle(mach->win, "zq-32");

  mach->tex = SDL_CreateTexture(mach->ren, SDL_PIXELFORMAT_RGB332,
                      SDL_TEXTUREACCESS_STREAMING, 320, 200);

  if(!mach->tex) {
    fprintf(stderr, "%s\n", SDL_GetError());
    exit(EXIT_FAILURE);
  }

  return mach;
}

bool zq32_loop(struct zq32* mach) {
  static int cycle = 0x00;
  SDL_Event e;


  if(!(cycle++ & 0xF)) {
    if(SDL_UpdateTexture(mach->tex, NULL, mach->vram, 320)) {
      fprintf(stderr, "%s\n", SDL_GetError());
      exit(EXIT_FAILURE);
    }

    SDL_RenderCopy(mach->ren, mach->tex, NULL, NULL);
    SDL_RenderPresent(mach->ren);

    while(SDL_PollEvent(&e)) {
      if(e.type == SDL_QUIT) {
        return false;
      }
      if(e.type == SDL_KEYDOWN) {
        mach->kbd_buff[mach->kbd_top++] = e.key.keysym.sym;
      }
    }
  }

  uint32_t in_raw = mmio_read(mach, mach->ri);
  struct zq32_instruction in = *(struct zq32_instruction*) &in_raw;

  if(in.is_reg) {
    mach->op2 = mach->r[in.src2];
  } else {
    mach->op2 = in.raw;
  }

  dbg_printf(stderr, "0x%.8x: %s r%u = r%u (0x%.8x), %d\n", mach->ri, opcode_str[in.opcode], in.dst, in.src, mach->r[in.src], mach->op2);

  mach->ri += 4;

  switch(in.cond) {
    case COND_ALWAYS:
      zq32_handler[in.opcode](mach, &in);
    break;
    case COND_NOT_NEG:
      if(!(mach->rf & FLAG_NEG))
        zq32_handler[in.opcode](mach, &in);
    break;
    case COND_POSITIVE:
      if(!((mach->rf & FLAG_NEG) | (mach->rf & FLAG_ZERO)))
        zq32_handler[in.opcode](mach, &in);
    break;
    case COND_NOT_ZERO:
      if(!(mach->rf & FLAG_ZERO))
        zq32_handler[in.opcode](mach, &in);
    break;
    case COND_NEGATIVE:
      if(mach->rf & FLAG_NEG)
        zq32_handler[in.opcode](mach, &in);
    break;
    case COND_ZERO:
      if(mach->rf & FLAG_ZERO)
        zq32_handler[in.opcode](mach, &in);
    break;
    case COND_CARRY:
      if(mach->rf & FLAG_CARRY)
        zq32_handler[in.opcode](mach, &in);
    break;
    case COND_OVERFLOW:
      if(mach->rf & FLAG_OVRFLW)
        zq32_handler[in.opcode](mach, &in);
    break;
  }

  mach->r[0] = 0x00000000;

  return true;
}

void zq32_dump_reg(struct zq32* mach, FILE* out) {
  for(size_t i = 0; i < 12; i++) {
    fprintf(out, "r%lu\t=\t0x%.8X\n", i, mach->r[i]);
  }

  fprintf(out, "rf\t=\t0x%.8X { ZERO = %d, NEG = %d, CARRY = %d, OVRFLW = %d }\n",
    mach->rf, 
    !!(mach->rf & FLAG_ZERO),
    !!(mach->rf & FLAG_NEG),
    !!(mach->rf & FLAG_CARRY),
    !!(mach->rf & FLAG_OVRFLW)
  );

  fprintf(out, "ri\t=\t0x%.8X\n", mach->ri);
  fprintf(out, "rv\t=\t0x%.8X\n", mach->rv);
  fprintf(out, "rt\t=\t0x%.8X\n", mach->rt);
}
