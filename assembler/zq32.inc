element CC
ALWAYS? equ CC.[:000b:]
NN?     equ CC.[:001b:]
NZ?     equ CC.[:010b:]
P?      equ CC.[:011b:]
N?      equ CC.[:100b:]
Z?      equ CC.[:101b:]
C?      equ CC.[:101b:]
OV?     equ CC.[:111b:]

element R
r0?     equ R.0
r1?     equ R.1
r2?     equ R.2
r3?     equ R.3
r4?     equ R.4
r5?     equ R.5
r6?     equ R.6
r7?     equ R.7
r8?     equ R.8
r9?     equ R.9
r10?    equ R.10
r11?    equ R.11
rf?     equ R.12
ri?     equ R.13
rv?     equ R.14
r15?    equ R.15

; 0: ccc 3: oooo 7: xxxx 11: yyyy 15: 1 16: zzzz 20: 000000000000
; 0: ccc 3: oooo 7: xxxx 11: yyyy 15: 0 16: iiiiiiiiiiiiiiii
macro GUTS.INSTRUCTION flg, cc, op, dst, src1, src2
  dd cc or (op shl 3) or (dst shl 7) or (src1 shl 11) or (flg shl 15) or (src2 shl 16)
end macro

macro GUTS.arithmetic_factory name, OP
  macro name? arguements&
    local cc, dst, src1, src2, flg
    match CC.[:cond:] R._dst=, R._src1=, R._src2, arguements
      cc = cond
      dst = _dst
      src1 = _src1
      src2 = _src2
      flg = 1
    else match CC.[:cond:] R._dst=, R._src1=, imm, arguements
      cc = cond
      dst = _dst
      src1 = _src1
      src2 = imm
      flg = 0
    else match CC.[:cond:] R._dst=, R._src1, arguements
      cc = cond
      dst = _dst
      src1 = _dst
      src2 = _src1
      flg = 1
    else match CC.[:cond:] R._dst=, imm, arguements
      cc = cond
      dst = _dst
      src1 = _dst
      src2 = imm
      flg = 0
    else match R._dst=, R._src1=, R._src2, arguements
      cc = 0
      dst = _dst
      src1 = _src1
      src2 = _src2
      flg = 1
    else match R._dst=, R._src1=, imm, arguements
      cc = 0
      dst = _dst
      src1 = _src1
      src2 = imm
      flg = 0
    else match R._dst=, R._src1, arguements
      cc = 0
      dst = _dst
      src1 = _dst
      src2 = _src1
      flg = 1
    else match R._dst=, imm, arguements
      cc = 0
      dst = _dst
      src1 = _dst
      src2 = imm
      flg = 0
    else
      err "invalid combination of instruction and operands"
    end match

    GUTS.INSTRUCTION flg, cc, OP, dst, src1, src2
  end macro
end macro


GUTS.arithmetic_factory ADD, 0x00
GUTS.arithmetic_factory SUB, 0x01
GUTS.arithmetic_factory AND, 0x02
GUTS.arithmetic_factory  OR, 0x03
GUTS.arithmetic_factory XOR, 0x04
GUTS.arithmetic_factory LOD, 0x05
GUTS.arithmetic_factory STR, 0x06
GUTS.arithmetic_factory SHL, 0x07
GUTS.arithmetic_factory SHR, 0x08
GUTS.arithmetic_factory ROL, 0x09
GUTS.arithmetic_factory ROR, 0x0A
GUTS.arithmetic_factory LDD, 0x0B
GUTS.arithmetic_factory LDB, 0x0C
GUTS.arithmetic_factory STD, 0x0D
GUTS.arithmetic_factory STB, 0x0E
GUTS.arithmetic_factory SIX, 0x0F

macro set? arguements&
  local cc, args
  match CC.[:cond:] R.dst=, src, arguements
    add CC.[:cond:] R.dst, r0, src
  else match R.dst=, src, arguements
    add R.dst, r0, src
  else
    err "invalid combination of instruction and operands"
  end match
end macro

macro jmp? arguements&
  local cc, args
  match CC.[:cond:] args, arguements
    add CC.[:cond:] ri, r0, args
  else match CC.[:cond:] =rel? args, arguements 
    add CC.[:cond:] ri, args
  else match =rel? args, arguements
    add ri, args
  else match args, arguements
    add ri, r0, args
  else match CC.[:cond:], arguements
    err "expected address"
  else
    err "invalid combination of instruction and operands"
  end match
end macro

macro hlt? cc:ALWAYS
  jmp cc ri
end macro