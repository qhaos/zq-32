include '../assembler/zq32.inc'


set r3, 200*320
set r2, 0xFFFF
shl r2, 16

or r2, 0xFFFF
kbd_wait:
  lod r4, r2, 0
  jmp z kbd_wait

xor r5, r4, 0x1337
ror r4, 8

shl r2, 16

loop:
  ror r1, 3
  xor r1, r4
  xor r1, r2
  str r1, r2, r0
  add r2, 4
  sub r3, 4
  ror r4, r1
  jmp nz loop

jmp $
