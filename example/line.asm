include "../assembler/zq32.inc"

set r2, 0xFFFF
shl r2, 16

; here we will use r3 as our counter
set r3, 320*100
loop:
  stb r3, r2, r3
  sub r3, 7
  jmp NZ loop


jmp $

