Qh4os's zq-32 assembler and machine manual v0

A manual for the zq-32 emulator and bundled assembler


Bulding and running on a posix compliant shell (sh, bash, etc)
```sh
# install SDL2 first
# to build the main program:
cc src/* -Iinclude -lsdl2 -o main

# to run an example:
./main example/line

# to assemble your own example: (grab fasmg.exe if on windows)
fasmg.x64 path/to/code.asm -o path/to/output
# to run said code
./main path/to/output
```

Table of Contents

1. Introduction
  - Machine Description
  - Instruction Encoding
2. List of operations and behavior
3. Device interface


1. Introduction

Machine Description -

  All registers are 32-bits, machine is Little Endian

  Registers:
  +---------------+-----------------------+
  | register name |       description     |
  +---------------+-----------------------+
  |       r0      |     always reads 0    |
  |    r1 - r11   |     general purpose   |
  |       rf      |         flags         |
  |       ri      |   instruction pointer |
  |       rv      |      link register    |
  |       r15     |      general purpose  | 
  +---------------+-----------------------+

  Flags Encoding -
  
    znco0000000000000000000000000000
  
    z = zero
    n = negative
    c = carry
    o = overflow

Instruction Encoding -
  Instructions have 2 possible encodings,
    if bit 15 = 1, then the second half (bits 16-31) holds a third register index,
    otherwise, the second half (bit 16-31) holds a signed 16-bit integer 

                    1 zzzz000000000000
  cccooooxxxxyyyy -|
                    0 iiiiiiiiiiiiiiii




2. List of operations and behavior

  Instructions are written as: `op CC dst, src1, src2`,
    where CC is a condition code (see table below), and if not present, is assumed to be ALWAYS,
    dst is a register (usually used as the destination),
    src1 is a register (usually the source)
    src2 is either a register or an immediate

  The flags register is tested against this, and if the conditons are met, the instruction is executed

  Table of Condition Codes (case insensitive)
  | Name   | Encoding | ZF | NF | CF | OF |
  | ALWAYS | 000      | ?  | ?  | ?  | ?  |
  | NN     | 001      | ?  | 0  | ?  | ?  |
  | NZ     | 010      | 0  | ?  | ?  | ?  |
  | P      | 011      | 0  | 0  | ?  | ?  |
  | N      | 100      | 0  | 1  | ?  | ?  |
  | Z      | 101      | 1  | 0  | ?  | ?  |
  | C      | 110      | ?  | ?  | 1  | ?  |
  | OV     | 111      | ?  | ?  | ?  | 1  |


  Operations table - Quick refrence

  | Opcode | Name |
  | 0b0000 | ADD  |
  | 0b0001 | SUB  |
  | 0b0010 | AND  |
  | 0b0011 | OR   |
  | 0b0100 | XOR  |
  | 0b0101 | LOD  |
  | 0b0110 | STR  |
  | 0b0111 | SHL  |
  | 0b1000 | SHR  |
  | 0b1001 | ROL  |
  | 0b1010 | ROR  |
  | 0b1011 | LDD  |
  | 0b1100 | LDB  |
  | 0b1101 | STD  |
  | 0b1110 | STB  |
  | 0b1111 | SIX  |

  Operation Breakdown:

    | 0b0000 | ADD |

      sets dst equal to the sum of src1 and src2

      Syntax example:

      ; 3 pronged form
      add r1, r2, r3   ; r1 <- r2 + r3

      ; shortened form
      add r1, r2       ; r1 <- r1 + r2

      Flag Affection:
        Zero, Carry, Overflow, Negative

    | 0b0001 | SUB |

      sets dst equal to the difference of src1 and src2

      Syntax example:

      ; 3 pronged form
      sub r1, r2, r3   ; r1 <- r2 - r3

      ; shortened form
      sub r1, r2       ; r1 <- r1 - r2

      Flag Affection:
        Zero, Carry, Overflow, Negative


    | 0b0010 | AND |

      sets dst equal to the binary AND of src1 and src2

      Syntax example:

      ; 3 pronged form
      and r1, r2, r3   ; r1 <- r2 & r3

      ; shortened form
      and r1, r2       ; r1 <- r1 & r2

      Flag Affection:
        Zero, Negative

    | 0b0011 | OR |

      sets dst equal to the binary OR of src1 and src2

      Syntax example:

      ; 3 pronged form
      or r1, r2, r3   ; r1 <- r2 | r3

      ; shortened form
      or r1, r2       ; r1 <- r1 | r2

      Flag Affection:
        Zero, Negative
        
    | 0b0100 | XOR |
    
      sets dst equal to the binary XOR of src1 and src2

      Syntax example:

      ; 3 pronged form
      xor r1, r2, r3   ; r1 <- r2 ^ r3

      ; shortened form
      xor r1, r2       ; r1 <- r1 ^ r2

      Flag Affection:
        Zero, Negative


    | 0b0101 | LOD |
    
      load 32-bits from address src1 + src2 into dst

      Syntax example:

      ; 3 pronged form
      lod r1, r2, r3   ; r1 <- [r2 + r3]

      ; shortened form
      lod r1, r2       ; r1 <- [r1 + r2]

      Flag Affection:
        Zero

    | 0b0110 | STR |

      store 32-bit dst into the address src1 + src2

      Syntax example:

      ; 3 pronged form
      str r1, r2, r3   ; [r2 + r3] <- r1

      ; shortened form
      str r1, r2       ; [r1 + r2] <- r1

      Flag Affection:
          None

    | 0b0111 | SHL |

      set dst equal to src1 shifted left src2 times

      Syntax example:

      ; 3 pronged form
      shl r1, r2, r3   ; r1 <- r2 << r3

      ; shortened form
      shl r1, r2       ; r1 <- r1 << r2

      Flag Affection:
        CARRY (shifts into carry before falling)

        
    | 0b1000 | SHR |

      set dst equal to src1 shifted right src2 times

      Syntax example:

      ; 3 pronged form
      shr r1, r2, r3   ; r1 <- r2 >> r3

      ; shortened form
      shr r1, r2       ; r1 <- r1 >> r2

      Flag Affection:
        CARRY (shifts into carry before falling)

    | 0b1001 | ROL |

      set dst equal to src1 rotated left src2 times

      Syntax example:

      ; 3 pronged form
      rol r1, r2, r3   ; r1 <- r2 rol r3 

      ; shortened form
      rol r1, r2       ; r1 <- r1 rol r2

      Flag Affection:
        None

        
    | 0b1010 | ROR |

      set dst equal to src1 rotated right src2 times

      Syntax example:

      ; 3 pronged form
      ror r1, r2, r3   ; r1 <- r2 ror r3 

      ; shortened form
      ror r1, r2       ; r1 <- r1 ror r2

      Flag Affection:
        None

    | 0b1011 | LDD |

      load 16-bits from address src1 + src2 into dst

      Syntax example:

      ; 3 pronged form
      ldd r1, r2, r3   ; r1 <- [r2 + r3]

      ; shortened form
      ldd r1, r2       ; r1 <- [r1 + r2]

      Flag Affection:
        Zero

    | 0b1100 | LDB |

      load 8-bits from address src1 + src2 into dst

      Syntax example:

      ; 3 pronged form
      ldd r1, r2, r3   ; r1 <- [r2 + r3]

      ; shortened form
      ldd r1, r2       ; r1 <- [r1 + r2]

      Flag Affection:
        Zero


    | 0b1101 | STD |

      store lower 16-bits of dst at address src1 + src2

      Syntax example:

      ; 3 pronged form
      std r1, r2, r3   ; [r2 + r3] <- r1

      ; shortened form
      std r1, r2       ; [r1 + r2] <- r1

      Flag Affection:
          None
    
    | 0b1110 | STB |

      store lower 8-bits of dst at address src1 + src2

      Syntax example:

      ; 3 pronged form
      stb r1, r2, r3   ; [r2 + r3] <- r1

      ; shortened form
      stb r1, r2       ; [r1 + r2] <- r1

      Flag Affection:
          None

    For time reasons, definition of SIX was not rewritten, here is the original:

    1111 Six: A = [B six C], if C = 0 then sign extends from 8 bit to 32, if C = 1 sign extends from 16 bit to 32 bit, if C = 2 it sign preserves from 32 bit to 16, if C = 3 it sign preserves from 16 bit to 8 bit


3. Device interface

  IO is entirely MMIO right now, and has access to two devices, an RGB framebuffer, and a keyboard

  - Keyboard
    
    A queue of SDL Sym codes are stored in a queue, reading from address 0xFFFFFFFF will pop an element from that queue

    If there is no element, 0 will be read.

  - Framebuffer

    Raw RGB332 pixel buffer of size 320 * 200 located at 0xFFFF0000 - 0xFFFFFA00


