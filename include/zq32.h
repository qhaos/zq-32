#pragma once
#include <stdbool.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>
#include <SDL2/SDL.h>

struct zq32 {
  /* registers */
  uint32_t   r[16];

  uint8_t   ram[(1 << 20) + 4];
  uint8_t   vram[320 * 200 + 4];
  uint8_t   kbd_buff[256];
  uint8_t   kbd_top;
  uint8_t   kbd_bottom;
  uint32_t  op2;

  SDL_Window* win;
  SDL_Renderer* ren;
  SDL_Texture* tex;
};

struct zq32_instruction;

struct zq32*  zq32_new();
bool          zq32_loop(struct zq32*);
void          zq32_dump_reg(struct zq32*, FILE*);
