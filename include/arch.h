#pragma once
#include <stdint.h>
#include "zq32.h"

#define pstruct struct __attribute__((packed))

enum {
  FLAG_ZERO   =     (1 << 0),
  FLAG_NEG    =     (1 << 1),
  FLAG_CARRY  =     (1 << 2),
  FLAG_OVRFLW =     (1 << 3),
};

enum condition {
  COND_ALWAYS,
  COND_NOT_NEG,
  COND_NOT_ZERO,
  COND_POSITIVE,
  COND_NEGATIVE,
  COND_ZERO,
  COND_CARRY,
  COND_OVERFLOW,
};

/* register renames */
#define rf r[12]
#define ri r[13]
#define rv r[14]
#define rt r[15]

enum opcode {
  OP_ADD,
  OP_SUB,
  OP_AND,
  OP_OR,
  OP_XOR,
  OP_LOD,
  OP_STR,
  OP_SHL,
  OP_SHR,
  OP_ROL,
  OP_ROR,
  OP_LDD,
  OP_LDB,
  OP_STD,
  OP_STB,
  OP_SIX,
};

static const char* const opcode_str[] = {
  "OP_ADD",
  "OP_SUB",
  "OP_AND",
  "OP_OR",
  "OP_XOR",
  "OP_LOD",
  "OP_STR",
  "OP_SHL",
  "OP_SHR",
  "OP_ROL",
  "OP_ROR",
  "OP_LDD",
  "OP_LDB",
  "OP_STD",
  "OP_STB",
  "OP_SIX",
};

pstruct zq32_instruction {
  enum condition  cond    :  3;
  enum opcode     opcode  :  4;
  uint8_t         dst     :  4;
  uint8_t         src     :  4;
  uint8_t         is_reg  :  1;
  union {
    uint16_t      src2    : 16;
    uint16_t       raw     : 16;
  };
};

typedef void (*zq32_instruction_handler)(struct zq32*, struct zq32_instruction*);

uint32_t mmio_read(struct zq32*, uint32_t);
void mmio_write32(struct zq32* mach, uint32_t addr, uint32_t  val);
void mmio_write16(struct zq32* mach, uint32_t addr, uint16_t  val);
void mmio_write8 (struct zq32* mach, uint32_t addr, uint8_t   val);
